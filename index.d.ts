/**
 * react-optimized-filter v3.0.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-optimized-filter/blob/master/LICENSE)
 */

import { DependencyList, ReactNode } from 'react';



export default function OptimizedFilter<T, TArg = undefined>(props: OptimizedFilterProps<T, TArg>): ReactNode;


export type OptimizedFilterProps<T, TArg> = {

  /**
   * List of items to be filtered.
   */
  items: T[];


  /**
   * Function used to filter {@link items}.
   */
  predicate: OptimizedFilterPredicate<T, TArg>;


  /**
   * An optional argument to be passed to the {@link predicate} function.
   */
  predicateArg?: TArg;


  /**
   * When truthy, filtering will be skipped.
   */
  skip?: any;


  /**
   * Extra dependencies, apart from the props of this component, that should trigger a re-filter.
   */
  extraDeps?: DependencyList;


  /**
   * Function to which the filtered list will be passed.
   */
  children(filteredItems: OptimizedFilterProps<T, TArg>['items']): ReactNode;
};



/**
 * @param items The list of items to be filtered.
 * @param predicate The function used to filter {@link items}.
 * @param extraDeps Extra dependencies, apart from the arguments of this hook, that should trigger a re-filter.
 */
export function useOptimizedFilter<T>(
  items: T[],
  predicate: OptimizedFilterPredicate<T>,
  extraDeps?: DependencyList
): T[];

/**
 * @param items The list of items to be filtered.
 * @param predicate The function used to filter {@link items}.
 * @param options Hook options.
 * @param extraDeps Extra dependencies, apart from the arguments of this hook, that should trigger a re-filter.
 */
export function useOptimizedFilter<T, TArg = undefined>(
  items: T[],
  predicate: OptimizedFilterPredicate<T, TArg>,
  options: UseOptimizedFilterOptions<TArg> | null | undefined,
  extraDeps?: DependencyList
): T[];


export type UseOptimizedFilterOptions<TArg> = {

  /**
   * An optional argument to be passed to the predicate function.
   */
  predicateArg?: TArg;


  /**
   * When truthy, filtering will be skipped.
   */
  skip?: any;
}


/**
 * @param item - The item to be filtered.
 * @param index - The index of the item in the list.
 * @param arg - The {@link predicateArg} prop passed to the component/hook.
 *
 * @returns truthy/falsey value. If truthy, the item will be included in the filtered list.
 */
export type OptimizedFilterPredicate<T, TArg = undefined> = (item: T, index: number, arg?: TArg) => any;
