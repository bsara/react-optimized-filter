/**
 * react-optimized-filter v3.0.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-optimized-filter/blob/master/LICENSE)
 */

import { useMemo } from 'react';

import { MD5 } from 'object-hash';



export default function OptimizedFilter({ children, extraDeps, items, predicate, predicateArg, skip }) {
  return children(
    useOptimizedFilter(items, predicate, { predicateArg, skip }, extraDeps)
  );
}



export function useOptimizedFilter(items, predicate, options, extraDeps) {
  const { predicateArg, skip } = (typeof options === 'object' ? options : {});
  const finalExtraDeps = ((Array.isArray(options) ? options : extraDeps) ?? []);

  return useMemo(
    () => {
      if (skip || !items?.length) {
        return items;
      }

      const ret = [];

      for (let i = 0; i < items.length; i++) {
        const item = items[i];

        if (predicate(item, i, predicateArg)) {
          ret.push(item);
        }
      }

      return ret;
    },
    [
      MD5(items),
      MD5(predicate),
      (predicateArg && typeof predicateArg === 'object' ? MD5(predicateArg) : predicateArg),
      Boolean(skip),
      ...finalExtraDeps
    ]
  );
}
