# react-optimized-filter [![NPM Package](https://img.shields.io/npm/v/react-optimized-filter.svg?style=flat-square)][npm]

![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)

> A simple React component & hook used for array filtering in an optimized way.

> **NOTE:** This component/hook uses a for-loop for filtering rather than the native
> `Array`'s `filter` function. This means that filtering will be more performant *([see
> jsperf test for details](https://jsperf.com/filter-func-vs-for-loop))*.

[Storybook](https://bsara.gitlab.io/react-optimized-filter)

[Changelog](https://gitlab.com/bsara/react-optimized-filter/blob/master/CHANGELOG.md)


## In what way is this component/hook "optimized"?

This component uses a for-loop for filtering rather than the native `Array`'s `filter`
function, which yields better performance.

Re-filters only occur if the given items have changed order or any of the items have been
changed/removed/added since the previous render or if any of the predicate props have
changed since the last render. A memoize once pattern is used to accomplish this _(I.E.
the previous result of filtering is cached and filtering only occurs again if the
previously given props have changed, otherwise, the cached result is passed on to the
`children` prop)_.

**It is important to note that a hash of `items`, `predicate`, and the predicate args
is used to determined if a change has occurred.** This is very useful because one does
not need to ensure that the exact same objects are passed to `OptimizedFilter` with every
re-render; instead, everything can be specified inline, and you will still get all the
optimization benefits of `OptimizedFilter`. To illustrate, see the usage sections below.
In the examples given, only the first render of `MyComponent` will trigger filtering
within `OptimizedFilter`, while all subsequent renders will not trigger re-filtering even
though a new function is created and passed as the `predicate` prop with every render of
`MyComponent`.


## Install

```bash
$ npm i --save react-optimized-filter
```

## Usage

### Hook Usage

```tsx
import { useOptimizedFilter } from 'react-optimized-filter';

export default function MyComponent(props) {
  const filteredItems = useOptimizedFilter(
    props.items,
    (item, index, predicateArg) => {
      // Determine if item should be included in filtered list...
    },
    { predicateArg: props.predicateArg }
  );

  return (
    // Render filtered items...
  );
}
```

### Component Usage

```tsx
import OptimizedFilter from 'react-optimized-filter';

export default function MyComponent(props) {
  return (
    {/* ... */}
      <OptimizedFilter
        items={props.items}
        predicate={(item, index, predicateArg) => {
          // Determine if item should be included in filtered list...
        }}
        predicateArg={props.predicateArg}
      >
        {(filteredItems) => (
          // Render filtered items...
        )}
      </OptimizedFilter>
    {/* ... */}
  );
}
```


## Component Props

- **items** `T[]`

  Array of items to be filtered and passed to `children` prop.

- **predicate** `(item: T, itemIndex: number, predicateArg?: TArg) => truthy | falsey` - **REQUIRED**

  Function to be called for each item in `items`. Return a truthy value to include `item`
  in the resulting filtered items list. Return a falsey value to exclude `item` from the
  resulting filtered items list.

- **children** `(filteredItems: T[]) => React.ReactNode` - **REQUIRED**

  Function to which the filtered list will be passed.

- **predicateArg** `TArg`

  Parameter to be passed as an extra argument in `predicate` function. This prop is not
  required and is provided solely for convenience.

- **skip** `truthy | falsey`

  When `true`, skips filtering and the value of `items` prop will be passed to
  `children`, unsorted. This is useful in the case where sorting may only be
  conditionally needed.

- **extraDeps** `React.DependencyList`

  Extra dependencies, apart from the props of this component, that should trigger a
  re-filter.


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2024, Brandon D. Sara (https://bsara.dev)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[license]: https://gitlab.com/bsara/react-optimized-filter/blob/master/LICENSE "License"
[npm]:     https://www.npmjs.com/package/react-optimized-filter                "NPM Package: react-optimized-filter"
