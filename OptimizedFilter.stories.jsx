/** !
 * ISC License (ISC)
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

import React, { useEffect, useRef, useState } from 'react';

import escapeStringRegexp from 'escape-string-regexp';

import OptimizedFilter from '.';
import { action } from '@storybook/addon-actions';



/** @type {import('@storybook/react').Meta<typeof OptimizedFilter>} */
const meta = {
  component: OptimizedFilter
};
export default meta;



const items = [
  'apple',
  'apricot',
  'banana',
  'blackberry',
  'blueberry',
  'boysenberry',
  'cantaloupe',
  'cherry',
  'clementine',
  'fig',
  'gooseberry',
  'grape',
  'grapefruit',
  'guava',
  'honeydew',
  'huckleberry',
  'kiwi',
  'lemon',
  'lime',
  'mango',
  'nectarine',
  'orange',
  'papaya',
  'passion fruit',
  'peach',
  'pear',
  'pineapple',
  'plum',
  'pomegranate',
  'raspberry',
  'strawberry',
  'tangerine',
  'watermelon'
];


const filterOccurredAction = action('Filter Occurred');



/** @type {import('@storybook/react').StoryObj<typeof OptimizedFilter>} */
export const Default = {
  render: () => {
    const [ unrelatedValue, setUnrelatedValue ] = useState(false);

    const [ filterText, setFilterText ] = useState();
    const [ skip, setSkip ] = useState(false);

    const prevFilteredItems = useRef();

    return (
      <div>
        <div>
          <a href="https://gitlab.com/bsara/react-optimized-filter/blob/master/OptimizedFilter.stories.jsx#L109-129">View story source</a>
        </div>
        <br />
        <div>
          <div>
            <input id="unrelatedValue" type="checkbox" value={unrelatedValue} onChange={() => setUnrelatedValue(!unrelatedValue)} />
            <label htmlFor="unrelatedValue">&nbsp;Toggle this checkbox and look at the "Actions" panel to see that filters do not reoccur when there are no relevant changes.</label>
          </div>
          <div>
            <input id="skipCheckbox" type="checkbox" onChange={() => setSkip(!skip)} />
            <label htmlFor="skipCheckbox">&nbsp;Skip Filtering</label>
          </div>
          <br />
          <div>
            <input type="text" placeholder="Filter..." defaultValue={filterText} onChange={(e) => setFilterText(e.target.value)} />
          </div>
        </div>
        <hr />
        <div>
          <OptimizedFilter
            items={items}
            skip={skip}
            predicateArg={filterText}
            predicate={(item, index, arg) => (
              (arg == null || arg === '' || RegExp(escapeStringRegexp(arg), 'iu').test(item))
            )}
          >
            {(filteredItems) => {
              if (prevFilteredItems.current !== filteredItems) {
                prevFilteredItems.current = filteredItems;
                filterOccurredAction(filteredItems);
              }

              return (
                <React.Fragment key="filtered-items-fragment">
                  {filteredItems.map((item) => <div key={item}>{item}</div>)}
                </React.Fragment>
              );
            }}
          </OptimizedFilter>
        </div>
      </div>
    );
  }
};
Default.storyName = "OptimizedFilter";
