/** @type { import('@storybook/react-vite').StorybookConfig } */
const config = {
  stories: [
    '../*.stories.jsx'
  ],
  addons: [
    '@storybook/addon-essentials'
  ],
  framework: {
    name: '@storybook/react-vite',
    options: {}
  }
};
export default config;
