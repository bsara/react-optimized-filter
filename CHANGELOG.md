# 3.0.0

- **[BREAKING CHANGE]** Updated `react` min version to `17`.
- **[BREAKING CHANGE]** No longer publishing commonjs module, ESM only is provided now.
- **[BREAKING CHANGE]** Removed `render` prop from `OptimizedFilter` component, only `children` is used now.
- **[NEW]** Added `useOptimizedFilter` hook.
- **[NEW]** `predicate` is now hashed and checked for changes automatically.
- **[NEW]** Added typescript type definitions.
- **[DEPENDENCY UPDATE]** Updated `object-hash` dependency to version `^3.0.0`.
- Updated dev dependencies.


# 2.0.1

- **[DEPENDENCY UPDATE]** Updated `object-hash` dependency to version `2.0.3`.
- Updated dev dependencies.


# 2.0.0

- **[BREAKING CHANGE]** Class syntax is now used without being transpiled.
- **[DEPENDENCY UPDATE]** Updated `object-hash` dependency to version `2.0.1`.
- Updated some file names and converted to using `rollup` for build.
- Upgraded to latest versions of dev dependencies.


# 1.0.0

- Initial release. (forked from `@bsara/react-filter`)
